class AttemptedRemark(RuntimeError):
    pass


class GameOver(RuntimeError):
    pass


class Grid:
    grid_size = 9

    def __init__(self):
        self.grid = [None] * self.grid_size
        self.game_over = False

    def mark_square(self, square, mark):
        i = self.index_from_input(square)
        if self.grid[i] is not None:
            raise AttemptedRemark
        self.check_for_corrupted_board()
        self.grid[i] = mark
        self.check_for_game_over()

    def index_from_input(self, input):
        i = input - 1
        if not 0 <= i < self.grid_size:
            raise IndexError
        return i

    def check_for_game_over(self):
        for i in range(0, 7, 3):
            square = self.grid[i:i+3]
            if None not in square and square[0] == square[1] == square[2]:
                raise GameOver(square[0])
        for i in range(0, 3):
            square = self.grid[i], self.grid[i+3], self.grid[i+6],
            if None not in square and square[0] == square[1] == square[2]:
                raise GameOver(square[0])
        square = self.grid[0], self.grid[4], self.grid[8],
        if None not in square and square[0] == square[1] == square[2]:
            raise GameOver(square[0])
        square = self.grid[2], self.grid[4], self.grid[6],
        if None not in square and square[0] == square[1] == square[2]:
            raise GameOver(square[0])
        if None not in self.grid:
            raise GameOver

    def check_for_corrupted_board(self):
        try:
            self.check_for_game_over()
        except GameOver:
            raise ValueError
